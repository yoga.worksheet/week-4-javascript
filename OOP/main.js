class Table {
	constructor(initialData) {
		this.data = initialData;
	}

	createHead() {
		let items = this.data.headData.reduce((result, current) => {
			return result + `<th>${current}</th>`;
		}, "");
		return `<thead><tr>${items}</tr></thead>`;
	}

	createBody() {
		let items = this.data.bodyData.reduce((result, current) => {
			return (
				result +
				"<tr>" +
				current.reduce(
					(summary, item) => summary + `<td>${item}</td>`,
					""
				) +
				"</tr>"
			);
		}, "");
		return `<tbody>${items}</tbody>`;
	}

	render(target) {
		let displayTable = `<table class="table table-hover">${this.createHead()}${this.createBody()}</table>`;
		target.innerHTML = displayTable;
	}
}

let dummy = {
	headData: ["#", "Name", "Email", "Address"],
	bodyData: [
		[1, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
		[2, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
		[3, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
		[4, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
	],
};

const root = document.getElementById("root");

let dataTable = new Table(dummy);
dataTable.render(root);
