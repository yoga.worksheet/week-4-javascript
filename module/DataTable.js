export class Table {
	constructor(initialData) {
		this.data = initialData;
	}

	createHead() {
		let items = this.data.headData.reduce((result, current) => {
			return result + `<th>${current}</th>`;
		}, "");
		return `<thead><tr>${items}</tr></thead>`;
	}

	createBody() {
		let items = this.data.bodyData.reduce((result, current) => {
			return (
				result +
				"<tr>" +
				current.reduce(
					(summary, item) => summary + `<td>${item}</td>`,
					""
				) +
				"</tr>"
			);
		}, "");
		return `<tbody>${items}</tbody>`;
	}

	render(target) {
		let displayTable = `<table class="table table-hover">${this.createHead()}${this.createBody()}</table>`;
		target.innerHTML = displayTable;
	}
}
