import { Table } from "./DataTable.js";

let dummy = {
	headData: ["#", "Name", "Email", "Address"],
	bodyData: [
		[1, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
		[2, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
		[3, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
		[4, "fulan bin fulan", "fulan@fulan.com", "Indonesia"],
	],
};

const root = document.getElementById("root");

let dataTable = new Table(dummy);
dataTable.render(root);
